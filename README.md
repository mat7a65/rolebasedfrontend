# Rolebasedbackend

This frontend-project belongs to the backend-project 'rolebasedbackend', where I implemented a backend-server 
using JWT for authorization.

Here I implemented the frontend-client using angular 9.0.6, TypeScript, HTML5, CSS3 and Bootstrap.
This project could be a starting point for further developments using JWT an fine-granulated role- and 
permissions-concepts.
