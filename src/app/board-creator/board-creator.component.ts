import { Component, OnInit } from '@angular/core';
import {UserService} from '../_services/user.service';

@Component({
  selector: 'app-board-creator',
  templateUrl: './board-creator.component.html',
  styleUrls: ['./board-creator.component.css']
})
export class BoardCreatorComponent implements OnInit {
  content = '';

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getCreatorBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }

}
