import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from './_services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showDeciderBoard = false;
  showCreatorBoard = false;
  username: string;


  constructor(private tokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showCreatorBoard = this.roles.includes('ROLE_CREATOR');
      this.showDeciderBoard = this.roles.includes('ROLE_DECODER');

      this.username = user.username;

    }
  }

  logout() {
    this.tokenStorageService.signout();
    window.location.reload();
  }

}
