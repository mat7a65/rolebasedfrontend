import { Component, OnInit } from '@angular/core';
import {UserService} from '../_services/user.service';

@Component({
  selector: 'app-board-decider',
  templateUrl: './board-decider.component.html',
  styleUrls: ['./board-decider.component.css']
})
export class BoardDeciderComponent implements OnInit {
  content = '';

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getDeciderBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }

}
