import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardDeciderComponent } from './board-decider.component';

describe('BoardDeciderComponent', () => {
  let component: BoardDeciderComponent;
  let fixture: ComponentFixture<BoardDeciderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardDeciderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardDeciderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
